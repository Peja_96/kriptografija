﻿namespace Kripto
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.selectEFileButton = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.encryptButton = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.selectDFileButton = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.decryptButton = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.logoutButton = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.userList = new System.Windows.Forms.ComboBox();
            this.newMessage = new System.Windows.Forms.Button();
            this.TabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // TabControl
            // 
            this.TabControl.Controls.Add(this.tabPage1);
            this.TabControl.Controls.Add(this.tabPage2);
            this.TabControl.Location = new System.Drawing.Point(12, 51);
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedIndex = 0;
            this.TabControl.Size = new System.Drawing.Size(500, 415);
            this.TabControl.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.selectEFileButton);
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Controls.Add(this.encryptButton);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(492, 385);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Encryption";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // selectEFileButton
            // 
            this.selectEFileButton.Location = new System.Drawing.Point(70, 198);
            this.selectEFileButton.Name = "selectEFileButton";
            this.selectEFileButton.Size = new System.Drawing.Size(113, 43);
            this.selectEFileButton.TabIndex = 2;
            this.selectEFileButton.Text = "select file";
            this.selectEFileButton.UseVisualStyleBackColor = true;
            this.selectEFileButton.Click += new System.EventHandler(this.selectEFileButton_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(70, 159);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(276, 23);
            this.textBox1.TabIndex = 1;
            // 
            // encryptButton
            // 
            this.encryptButton.Location = new System.Drawing.Point(70, 333);
            this.encryptButton.Name = "encryptButton";
            this.encryptButton.Size = new System.Drawing.Size(102, 46);
            this.encryptButton.TabIndex = 0;
            this.encryptButton.Text = "Encrypt";
            this.encryptButton.UseVisualStyleBackColor = true;
            this.encryptButton.Click += new System.EventHandler(this.encryptButton_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.selectDFileButton);
            this.tabPage2.Controls.Add(this.textBox2);
            this.tabPage2.Controls.Add(this.decryptButton);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(492, 385);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Decryption";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // selectDFileButton
            // 
            this.selectDFileButton.Location = new System.Drawing.Point(70, 198);
            this.selectDFileButton.Name = "selectDFileButton";
            this.selectDFileButton.Size = new System.Drawing.Size(113, 43);
            this.selectDFileButton.TabIndex = 4;
            this.selectDFileButton.Text = "select file";
            this.selectDFileButton.UseVisualStyleBackColor = true;
            this.selectDFileButton.Click += new System.EventHandler(this.selectDFileButton_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(70, 159);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(276, 23);
            this.textBox2.TabIndex = 3;
            // 
            // decryptButton
            // 
            this.decryptButton.Location = new System.Drawing.Point(70, 333);
            this.decryptButton.Name = "decryptButton";
            this.decryptButton.Size = new System.Drawing.Size(102, 46);
            this.decryptButton.TabIndex = 0;
            this.decryptButton.Text = "Decrypt";
            this.decryptButton.UseVisualStyleBackColor = true;
            this.decryptButton.Click += new System.EventHandler(this.decryptButton_Click);
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.SelectedPath = "C:\\Users\\Nikola\\Desktop";
            this.folderBrowserDialog1.ShowNewFolderButton = false;
            // 
            // logoutButton
            // 
            this.logoutButton.Location = new System.Drawing.Point(660, 55);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(128, 44);
            this.logoutButton.TabIndex = 1;
            this.logoutButton.Text = "Logout";
            this.logoutButton.UseVisualStyleBackColor = true;
            this.logoutButton.Click += new System.EventHandler(this.logoutButton_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.InitialDirectory = "C:\\Users\\Nikola\\Desktop\\kripto\\Kripto";
            // 
            // userList
            // 
            this.userList.FormattingEnabled = true;
            this.userList.Items.AddRange(new object[] {
            "Nikola",
            "Petar",
            "Marko"});
            this.userList.Location = new System.Drawing.Point(599, 237);
            this.userList.Name = "userList";
            this.userList.Size = new System.Drawing.Size(121, 25);
            this.userList.TabIndex = 2;
            this.userList.Text = "Nikola";
            // 
            // newMessage
            // 
            this.newMessage.Image = global::Kripto.Properties.Resources.poruka1;
            this.newMessage.Location = new System.Drawing.Point(746, 126);
            this.newMessage.Name = "newMessage";
            this.newMessage.Size = new System.Drawing.Size(42, 37);
            this.newMessage.TabIndex = 3;
            this.newMessage.UseVisualStyleBackColor = true;
            this.newMessage.Visible = false;
            this.newMessage.Click += new System.EventHandler(this.newMessage_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.ClientSize = new System.Drawing.Size(800, 478);
            this.Controls.Add(this.newMessage);
            this.Controls.Add(this.userList);
            this.Controls.Add(this.logoutButton);
            this.Controls.Add(this.TabControl);
            this.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Main";
            this.Text = "Cryptography";
            this.Load += new System.EventHandler(this.Main_Load);
            this.TabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl TabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button encryptButton;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button selectEFileButton;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button logoutButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button decryptButton;
        private System.Windows.Forms.Button selectDFileButton;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ComboBox userList;
        private System.Windows.Forms.Button newMessage;
    }
}