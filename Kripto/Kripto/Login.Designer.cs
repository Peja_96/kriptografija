﻿namespace Kripto
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.userBox = new System.Windows.Forms.TextBox();
            this.passBox = new System.Windows.Forms.TextBox();
            this.logButton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.username = new System.Windows.Forms.Label();
            this.password = new System.Windows.Forms.Label();
            this.certBox = new System.Windows.Forms.TextBox();
            this.certificate = new System.Windows.Forms.Label();
            this.certButton = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // userBox
            // 
            this.userBox.BackColor = System.Drawing.SystemColors.Window;
            this.userBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userBox.Location = new System.Drawing.Point(296, 116);
            this.userBox.Name = "userBox";
            this.userBox.Size = new System.Drawing.Size(237, 30);
            this.userBox.TabIndex = 0;
            // 
            // passBox
            // 
            this.passBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passBox.Location = new System.Drawing.Point(296, 188);
            this.passBox.Name = "passBox";
            this.passBox.Size = new System.Drawing.Size(237, 30);
            this.passBox.TabIndex = 1;
            this.passBox.UseSystemPasswordChar = true;
            // 
            // logButton
            // 
            this.logButton.Location = new System.Drawing.Point(328, 317);
            this.logButton.Name = "logButton";
            this.logButton.Size = new System.Drawing.Size(94, 49);
            this.logButton.TabIndex = 2;
            this.logButton.Text = "Login";
            this.logButton.UseVisualStyleBackColor = true;
            this.logButton.Click += new System.EventHandler(this.logButton_Click);
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(439, 317);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(94, 49);
            this.exitButton.TabIndex = 3;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // username
            // 
            this.username.AutoSize = true;
            this.username.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.username.ForeColor = System.Drawing.SystemColors.ControlText;
            this.username.Location = new System.Drawing.Point(193, 124);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(88, 22);
            this.username.TabIndex = 4;
            this.username.Text = "Username";
            // 
            // password
            // 
            this.password.AutoSize = true;
            this.password.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.password.Location = new System.Drawing.Point(193, 196);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(88, 22);
            this.password.TabIndex = 5;
            this.password.Text = "Password";
            // 
            // certBox
            // 
            this.certBox.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.certBox.Location = new System.Drawing.Point(296, 251);
            this.certBox.Name = "certBox";
            this.certBox.Size = new System.Drawing.Size(237, 28);
            this.certBox.TabIndex = 6;
            // 
            // certificate
            // 
            this.certificate.AutoSize = true;
            this.certificate.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.certificate.Location = new System.Drawing.Point(193, 258);
            this.certificate.Name = "certificate";
            this.certificate.Size = new System.Drawing.Size(86, 21);
            this.certificate.TabIndex = 7;
            this.certificate.Text = "Certificate";
            // 
            // certButton
            // 
            this.certButton.Location = new System.Drawing.Point(559, 251);
            this.certButton.Name = "certButton";
            this.certButton.Size = new System.Drawing.Size(109, 28);
            this.certButton.TabIndex = 8;
            this.certButton.Text = "Find cert";
            this.certButton.UseVisualStyleBackColor = true;
            this.certButton.Click += new System.EventHandler(this.certButton_Click);
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.SelectedPath = "C:\\Users\\Nikola\\Desktop\\kripto\\CA\\root\\ca\\intermediate\\certs";
            this.folderBrowserDialog1.ShowNewFolderButton = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.InitialDirectory = "C:\\Users\\Nikola\\Desktop\\kripto\\CA\\root\\ca\\intermediate\\users";
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.certButton);
            this.Controls.Add(this.certificate);
            this.Controls.Add(this.certBox);
            this.Controls.Add(this.password);
            this.Controls.Add(this.username);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.logButton);
            this.Controls.Add(this.passBox);
            this.Controls.Add(this.userBox);
            this.Name = "Login";
            this.Text = "Login";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox userBox;
        private System.Windows.Forms.TextBox passBox;
        private System.Windows.Forms.Button logButton;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Label username;
        private System.Windows.Forms.Label password;
        private System.Windows.Forms.TextBox certBox;
        private System.Windows.Forms.Label certificate;
        private System.Windows.Forms.Button certButton;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

