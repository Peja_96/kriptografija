﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kripto
{
    class ValidationException : Exception
    {
       public ValidationException() : base("Data is not valid") { }
       public ValidationException(string s) : base(s) { }
    }
}
