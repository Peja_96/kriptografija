﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Diagnostics;
using System.Threading;

using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Asn1.Pkcs;
using Org.BouncyCastle.X509;

namespace Kripto
{
    public partial class Main : Form
    {
        public static X509Certificate2 user1Cert;
        public static X509Certificate2 user2Cert;
        public static X509Certificate2 user3Cert;
        public static X509Certificate2 privateCert;
        public static string message;
        public static string selectedUser;
        public Main()
        {
            InitializeComponent();
        }

        private void selectEFileButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = openFileDialog1;
            if (ofd.ShowDialog() == DialogResult.OK)
                textBox1.Text = ofd.FileName;
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            Owner.Show();
            this.Close();
                       
        }

        private void encryptButton_Click(object sender, EventArgs e)
        {
            selectedUser = userList.SelectedItem.ToString();
            
            try
            {
                
                string filename = textBox1.Text;
                if (!filename.EndsWith(".cs"))
                    throw new Exception();
                string path = new FileInfo(filename).FullName;
                string key = GetKey();
                var parent = Directory.GetParent(path);
                var newPath = parent.FullName + "\\nova";
                Directory.CreateDirectory(newPath);
                EncryptFile(path, newPath, key);
                SignFile(newPath);
                message = selectedUser;
                MessageBox.Show("Message sent");
            }
            catch (InvalidKeyException)
            {
                MessageBox.Show("This certificate is not reliable");
            }
            catch (CryptographicException)
            {
                MessageBox.Show("Wrong key");
            }
            catch (ValidationException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {

                MessageBox.Show("Wrong path");
            }           
            
        }

        private void SignFile(string newPath)
        {
            var keyPair = DotNetUtilities.GetKeyPair(privateCert.PrivateKey);
            var PrivateKey = (RsaKeyParameters)keyPair.Private;
            byte[] bytes = File.ReadAllBytes(newPath + "\\kriptovano.txt");
            ISigner signer = SignerUtilities.GetSigner(PkcsObjectIdentifiers.Sha256WithRsaEncryption.Id);
            signer.Init(true, PrivateKey);
            signer.BlockUpdate(bytes, 0, bytes.Length);
            byte[] signature = signer.GenerateSignature();     
            string[] s = { Convert.ToBase64String(signature),":" };
            File.AppendAllLines(newPath + "\\info.txt", s);
            byte[] bytes1 = File.ReadAllBytes(newPath + "\\info.txt");
            ISigner signer1 = SignerUtilities.GetSigner(PkcsObjectIdentifiers.Sha256WithRsaEncryption.Id);
            signer.Init(true, PrivateKey);
            signer.BlockUpdate(bytes1, 0, bytes1.Length);
            byte[] signature1 = signer.GenerateSignature();
            string[] si = { Convert.ToBase64String(signature1) };
            File.AppendAllLines(newPath + "\\info.txt", si);
        }

        private void EncryptFile(string path, string newPath,  string key)
        {
            byte[] fileBytes = File.ReadAllBytes(path);
            
            using (var AES = new AesCryptoServiceProvider())
            {
                AES.IV = Encoding.UTF8.GetBytes(key);
                AES.Key = Encoding.UTF8.GetBytes(key);
                AES.Mode = CipherMode.CBC;
                AES.Padding = PaddingMode.PKCS7;
                using (var memStream =  new MemoryStream())
                {
                    CryptoStream cryptoStream = new CryptoStream(memStream, AES.CreateEncryptor(), CryptoStreamMode.Write);
                    cryptoStream.Write(fileBytes, 0, fileBytes.Length);
                    cryptoStream.FlushFinalBlock();
                    File.WriteAllBytes(newPath + "\\kriptovano.txt", memStream.ToArray());
                    EncryptKey(newPath,"AES", key);                    
                }
            }
        }

        private void EncryptKey(string path, string v, string key)
        {
            RSACryptoServiceProvider pub;
            if ("Nikola".Equals(selectedUser))
            {
                CheckCert(user1Cert);
                pub = (RSACryptoServiceProvider)user1Cert.PublicKey.Key;
            }
            else if (selectedUser.Equals("Petar"))
            {
                CheckCert(user2Cert);
                pub = (RSACryptoServiceProvider)user2Cert.PublicKey.Key;
            }
            else
            {
                CheckCert(user3Cert);
                pub = (RSACryptoServiceProvider)user3Cert.PublicKey.Key;
            }
            
            var encryptedData = pub.Encrypt(Encoding.UTF8.GetBytes(v), false);
            var encryptedKey = pub.Encrypt(Encoding.UTF8.GetBytes(key), false);
            string[] data = { Convert.ToBase64String(encryptedData),":", Convert.ToBase64String(encryptedKey),":" };
            File.WriteAllLines(path + "\\info.txt", data);
            
        }

        private string GetKey()
        {
            string key = "";
            string alfnum = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                while (key.Length != 16)
                {
                    byte[] oneByte = new byte[1];
                    rng.GetBytes(oneByte);
                    char c = (char)oneByte[0];
                    if (alfnum.Contains(c))
                        key += c;
                }
            }
            return key;
        }

        private void Main_Load(object sender, EventArgs e)
        {
            
            user1Cert = new X509Certificate2(@"C:\Users\Nikola\Desktop\kripto\CA\root\ca\intermediate\certs\user1.cert.cer");
            user2Cert = new X509Certificate2(@"C:\Users\Nikola\Desktop\kripto\CA\root\ca\intermediate\certs\user2.cert.cer");
            user3Cert = new X509Certificate2(@"C:\Users\Nikola\Desktop\kripto\CA\root\ca\intermediate\certs\user3.cert.cer");
            privateCert = new X509Certificate2(Login.certPath,Login.certKey,X509KeyStorageFlags.Exportable);
            if (Login.user.Equals(message))
                newMessage.Visible = true;
        }

        private void decryptButton_Click(object sender, EventArgs e)
        {
            selectedUser = userList.SelectedItem.ToString();
            X509Certificate2 cert;
            if ("Nikola".Equals(selectedUser))
            {
                cert = user1Cert;
            }
            else if ("Petar".Equals(selectedUser))
                cert = user2Cert;
            else
                cert = user3Cert;
            
            try
            {
                CheckCert(cert);
                string filename = textBox2.Text;
                if (!filename.EndsWith(".txt"))
                    throw new Exception();
                string path = new FileInfo(filename).FullName;
                var parent = Directory.GetParent(path);
                string[] lines = File.ReadAllLines(parent.FullName + "\\info.txt");
                ValidateInfo(parent.FullName + "\\info.txt", cert);
                ValidateData(path, lines[4], cert);
                string key = DecryptKey(lines[2]);               
                var newPath = parent.FullName;
                DecryptFile(path, newPath, key);
                message = "";
                ShowFile();
                
            }
            catch (InvalidKeyException)
            {
                MessageBox.Show("This certificate is not reliable");
            }
            catch (CryptographicException)
            {
                MessageBox.Show("Wrong key");
            }
            catch (ValidationException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {

                MessageBox.Show("Wrong path");
            }
            
        }

        private void CheckCert(X509Certificate2 cert) 
        {
            var inter = new X509Certificate2(@"C:\Users\Nikola\Desktop\kripto\CA\root\ca\intermediate\certs\intermediate.cert.cer");
            X509CertificateParser par = new X509CertificateParser();
            var newInter = par.ReadCertificate(inter.RawData);
            var newCert = par.ReadCertificate(cert.RawData);
            X509Crl crl = new X509CrlParser().ReadCrl(File.ReadAllBytes(@"C:\Users\Nikola\Desktop\kripto\CA\root\ca\intermediate\crl\intermediate.crl.pem"));
            newCert.Verify(newInter.GetPublicKey());
            crl.Verify(newInter.GetPublicKey());
            if (crl.IsRevoked(newCert))
                throw new ValidationException("Certificate has been revoked");
            if (!newCert.IsValidNow)
                throw new ValidationException("Certificate is not up to date");
            var usage = newCert.GetKeyUsage();
            if (!usage[3])
                throw new ValidationException("Certificate usage is not correct");
        }

        private void ShowFile()
        {
            var result = MessageBox.Show("Do you want to open file", "",
                                 MessageBoxButtons.YesNo,
                                 MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
                Compile();
        }

        private void Compile()
        {
            var proc1 = new ProcessStartInfo();
            string command = "/c csc dekriptovano.cs";
            proc1.UseShellExecute = true;
            proc1.WorkingDirectory = @"C:\Users\Nikola\Desktop\kripto\Kripto\nova";
            proc1.FileName = @"C:\Windows\SysWOW64\cmd.exe";           
            proc1.Arguments = command;
            var pro = new Process();
            pro.StartInfo = proc1;
            pro.Start();
            pro.WaitForExit();
            Process firstProc = new Process();
            firstProc.StartInfo.FileName = @"C:\Users\Nikola\Desktop\kripto\Kripto\nova\dekriptovano.exe";
            firstProc.EnableRaisingEvents = true;

            firstProc.Start();

            firstProc.WaitForExit();
        }

        private void ValidateInfo(string path, X509Certificate2 cert)
        {
            
            string[] lines = File.ReadAllLines(path);
            string signature = lines[lines.Length - 1];
            string temp = "";
            for (int i = 0; i < lines.Length - 1; i++)
            {
                temp += lines[i];
                temp += Environment.NewLine;
            }
            byte[] bytes = Encoding.ASCII.GetBytes(temp);
            ISigner signer = SignerUtilities.GetSigner(PkcsObjectIdentifiers.Sha256WithRsaEncryption.Id);
            signer.Init(false, DotNetUtilities.FromX509Certificate(cert).GetPublicKey());
            signer.BlockUpdate(bytes, 0, bytes.Length);
            bool status = signer.VerifySignature(Convert.FromBase64String(signature));
            if (!status)               
                throw new ValidationException();
        }

        private string DecryptKey(string v)
        {
            var priv = (RSACryptoServiceProvider)privateCert.PrivateKey;
            var decData =  priv.Decrypt(Convert.FromBase64String(v), false);
            return Encoding.UTF8.GetString(decData);
        }

        private void ValidateData(string path, string signature, X509Certificate2 cert)
        {
            
            var pub = DotNetUtilities.FromX509Certificate(cert);
            AsymmetricKeyParameter a = pub.GetPublicKey();
            //var key = user1Cert.PublicKey.Key
            byte[] bytes1 = File.ReadAllBytes(path);
            ISigner signer1 = SignerUtilities.GetSigner(PkcsObjectIdentifiers.Sha256WithRsaEncryption.Id);
            signer1.Init(false, pub.GetPublicKey());
            signer1.BlockUpdate(bytes1, 0, bytes1.Length);
            bool status = signer1.VerifySignature(Convert.FromBase64String(signature));
            if (!status)             
                throw new ValidationException("Content of the data has been changed");
        }

        private void DecryptFile(string path, string newPath, string key)
        {
            byte[] fileBytes = File.ReadAllBytes(path);
            using (var AES = new AesCryptoServiceProvider())
            {
                AES.IV = Encoding.UTF8.GetBytes(key);
                AES.Key = Encoding.UTF8.GetBytes(key);
                AES.Mode = CipherMode.CBC;
                AES.Padding = PaddingMode.PKCS7;
                using (var memStream = new MemoryStream())
                {
                    CryptoStream cryptoStream = new CryptoStream(memStream, AES.CreateDecryptor(), CryptoStreamMode.Write);
                    cryptoStream.Write(fileBytes, 0, fileBytes.Length);
                    cryptoStream.FlushFinalBlock();
                    File.WriteAllBytes(newPath + "\\dekriptovano.cs", memStream.ToArray());                   
                }
            }
        }

        private void selectDFileButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = openFileDialog1;
            if (ofd.ShowDialog() == DialogResult.OK)
                textBox2.Text = ofd.FileName;
        }

        private void newMessage_Click(object sender, EventArgs e)
        {
            newMessage.Visible = false;
        }
    }
}
