﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Security.Cryptography;

namespace Kripto
{
    public partial class Login : Form
    {
        public static string certPath;
        public static string certKey;
        public static string user;
        public Login()
        {
            InitializeComponent();
        }
        private void exitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void logButton_Click(object sender, EventArgs e)
        {
            String sifra = passBox.Text;
            byte[] sif = Encoding.ASCII.GetBytes(sifra);
            var sha = new SHA256CryptoServiceProvider();
            var hash = sha.ComputeHash(sif);
            var hashed = Convert.ToBase64String(hash);
            SqlConnection sql = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Nikola\Desktop\kripto\Kripto\Kripto\Users.mdf;Integrated Security=True;Connect Timeout=30");
            SqlDataAdapter adapter = new SqlDataAdapter("Select Count(*) From Tabela where Username='" + userBox.Text + "' and Password ='" + hashed + "' and Path ='" + certBox.Text + "'", sql);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            if (dt.Rows[0][0].ToString() == "1")
            {
                certPath = certBox.Text;
                certKey = passBox.Text;
                user = userBox.Text;
                Main main = new Main();
                main.Owner = this;
                main.Show();
                this.Hide();
            }
            else {
                MessageBox.Show("Authentication failed");
            }
        }
        private void certButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = openFileDialog1;
            if (ofd.ShowDialog() == DialogResult.OK)
                certBox.Text = ofd.FileName;
        }
    }
}
